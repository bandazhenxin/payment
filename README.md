# payment

## 说明

#### 介绍
一个使用老php版本的支付包，有很多废弃函数导致项目不能自由使用包，给它修改一下

#### 架构
不依赖php框架


## 使用

#### 安装

1.  composer require bdhert/payment
2.  开发版：composer require bdhert/payment:"dev-master"
3.  基础版：composer require bdhert/payment:"^0.1"

#### 配置

参考[原文档](https://gitee.com/helei112g/payment?_from=gitee_search#%E9%A1%B9%E7%9B%AE%E9%9B%86%E6%88%90)

#### 使用

参考[原文档](https://gitee.com/helei112g/payment?_from=gitee_search#%E9%A1%B9%E7%9B%AE%E9%9B%86%E6%88%90)
